#include "s21_credit.h"

void credit_annuity(double interest, double monthly, double *principal,
                    double *interest_paid, double *remaining_loan) {
  *principal = *remaining_loan * interest;
  *principal = round(*principal * 100) / 100;
  *interest_paid = monthly - *principal;
  *interest_paid = round(*interest_paid * 100) / 100;
  *remaining_loan -= *interest_paid;
  *remaining_loan = round(*remaining_loan * 100) / 100;
}

#include "s21_smartcalc.h"

void doubleinitialize(doubleStack *stack, int size) {
  stack->array = malloc(size * sizeof(double));
  stack->size = size;
  stack->top = -1;
}

int doubleisEmpty(doubleStack *stack) { return stack->top == -1; }

int doublepush(doubleStack *stack, double elem) {
  int error = 0;
  if (stack->top == stack->size - 1) {
    error = 1;
  } else
    stack->array[++stack->top] = elem;
  return error;
}

int doublepop(doubleStack *stack, double *elem) {
  int error = 0;
  if (doubleisEmpty(stack)) {
    error = 1;
  } else
    *elem = stack->array[stack->top--];
  return error;
}

int calcPostfix(char *postfixExpression, double *result) {
  doubleStack operandStack;
  int error = 0;
  char *endPtr;
  doubleinitialize(&operandStack, strlen(postfixExpression));
  for (int i = 0; postfixExpression[i] != '\0'; ++i) {
    char token = postfixExpression[i];
    if (isdigit(token) || (token == '.' && isdigit(postfixExpression[i + 1]))) {
      if (doublepush(&operandStack, strtod(&postfixExpression[i], &endPtr)) ==
          1)
        error = 1;
      while (isdigit(postfixExpression[i]) || postfixExpression[i] == '.') {
        ++i;
      }
      --i;
    } else if (token == ' ') {
      continue;
    } else {
      double operand2 = 0.0, operand1 = 0.0;
      switch (token) {
        case '+':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, operand1 + operand2) == 1) error = 1;
          break;
        case '-':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, operand1 - operand2) == 1) error = 1;
          break;
        case '*':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, operand1 * operand2) == 1) error = 1;
          break;
        case '/':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, operand1 / operand2) == 1) error = 1;
          break;
        case '^':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, pow(operand1, operand2)) == 1)
            error = 1;
          break;
        case 'm':
          if (doublepop(&operandStack, &operand2) == 1) error = 1;
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, fmod(operand1, operand2)) == 1)
            error = 1;
          break;
        case 's':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, sin(operand1)) == 1) error = 1;
          break;
        case 'c':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, cos(operand1)) == 1) error = 1;
          break;
        case 't':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, tan(operand1)) == 1) error = 1;
          break;
        case 'a':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, asin(operand1)) == 1) error = 1;
          break;
        case 'x':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, acos(operand1)) == 1) error = 1;
          break;
        case 'r':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, atan(operand1)) == 1) error = 1;
          break;
        case 'q':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, sqrt(operand1)) == 1) error = 1;
          break;
        case 'n':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, log(operand1)) == 1) error = 1;
          break;
        case 'o':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, log10(operand1)) == 1) error = 1;
          break;
        case 'k':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, operand1 * 1) == 1) error = 1;
          break;
        case 'l':
          if (doublepop(&operandStack, &operand1) == 1) error = 1;
          if (doublepush(&operandStack, -1 * operand1) == 1) error = 1;
          break;
        default:
          error = 1;
          break;
      }
    }
  }
  if (doublepop(&operandStack, result) == 1) error = 1;
  if (!(isfinite(*result))) error = 1;
  free(operandStack.array);
  return error;
}

#include "s21_smartcalc.h"

void initialize(Stack *stack, int size) {
  stack->array = malloc(size * sizeof(char));
  stack->size = size;
  stack->top = -1;
}

int isEmpty(Stack *stack) { return stack->top == -1; }

int push(Stack *stack, char elem) {
  int error = 0;
  if (stack->top == stack->size - 1) {
    error = 1;
  } else
    stack->array[++stack->top] = elem;
  return error;
}

int pop(Stack *stack, char *elem) {
  int error = 0;
  if (isEmpty(stack)) {
    error = 1;
  } else
    *elem = stack->array[stack->top--];
  return error;
}

int getPrecedence(char oper) {
  switch (oper) {
    case '+':
    case '-':
    case 'k':
    case 'l':
      return 1;
    case '*':
    case '/':
    case 'm':
      return 2;
    case '^':
      return 3;
    case 's':
    case 'c':
    case 't':
    case 'a':
    case 'x':
    case 'r':
      return 4;
    case 'n':
    case 'o':
      return 5;
    case 'q':
      return 6;
    default:
      return 0;
  }
}

int shuntingYard(char *infixExpression, char *postfixExpression) {
  Stack operatorStack;
  initialize(&operatorStack, strlen(infixExpression));
  char outputQueue[strlen(infixExpression) * 2], tempPop = '\0';
  int outputindex = 0, error = 0;
  for (int i = 0; infixExpression[i] != '\0'; ++i) {
    char token = infixExpression[i];
    if (isdigit(token) || (token == '.')) {
      while (isdigit(token) || token == '.') {
        outputQueue[outputindex++] = token;
        token = infixExpression[++i];
      }
      outputQueue[outputindex++] = ' ';
      i--;
    } else if (token == '(') {
      if (push(&operatorStack, token) == 1) error = 1;
    } else if (token == ')') {
      while (!isEmpty(&operatorStack) &&
             operatorStack.array[operatorStack.top] != '(') {
        if (pop(&operatorStack, &tempPop) == 1) error = 1;
        outputQueue[outputindex++] = tempPop;
        outputQueue[outputindex++] = ' ';
      }
      if (pop(&operatorStack, &tempPop) == 1) error = 1;
    } else if (strchr("+-*/^msctaxrqnokl", token) != NULL) {
      while (!isEmpty(&operatorStack) &&
             getPrecedence(token) <=
                 getPrecedence(operatorStack.array[operatorStack.top])) {
        if (pop(&operatorStack, &tempPop) == 1) error = 1;
        outputQueue[outputindex++] = tempPop;
        outputQueue[outputindex++] = ' ';
      }
      if (push(&operatorStack, token) == 1) error = 1;
    }
  }
  while (!isEmpty(&operatorStack)) {
    if (pop(&operatorStack, &tempPop) == 1) error = 1;
    outputQueue[outputindex++] = tempPop;
    outputQueue[outputindex++] = ' ';
  }
  if (outputindex > 0 && outputQueue[outputindex - 1] == ' ') {
    outputindex--;
  }
  outputQueue[outputindex] = '\0';
  strcpy(postfixExpression, outputQueue);
  if (postfixCheck(outputQueue) == 1) error = 1;
  free(operatorStack.array);
  return error;
}

int postfixCheck(char *postfixExpression) {
  int error = 0;
  for (int i = 0; postfixExpression[i] != '\0'; ++i) {
    char token = postfixExpression[i];
    if (token == '.') {
      if (strchr("1234567890", postfixExpression[i - 1]) == NULL) {
        error = 1;
        break;
      } else {
        while (1) {
          i++;
          if (isdigit(postfixExpression[i]))
            error = 0;
          else if (postfixExpression[i] == ' ') {
            error = 0;
            break;
          } else if (postfixExpression[i] == '.') {
            error = 1;
            break;
          }
        }
      }
      if (error == 1) break;
    }
  }
  return error;
}

int isUnary(char ch) { return ch == '+' || ch == '-'; }

int parser(char *infixExpression) {
  int error = 0;
  char output[255] = "";
  for (int i = 0, j = 0; infixExpression[i] != '\0'; ++i) {
    char token = infixExpression[i];
    if (isdigit(token) || (token == '.') || (token == ' ') || (token == '(') ||
        (token == ')') || (strchr("*/^", token) != NULL)) {
      output[j] = token;
      j++;
    } else if (token == '+' || token == '-') {
      if (i == 0 || (i > 0 && (isUnary(infixExpression[i - 1]) ||
                               infixExpression[i - 1] == '('))) {
        output[j++] = (token == '+') ? 'k' : 'l';
      } else {
        output[j++] = token;
      }
    } else if (isalpha(token)) {
      if ((token == 'm') && (infixExpression[i + 1] == 'o') &&
          (infixExpression[i + 2] == 'd')) {
        output[j++] = token;
        i += 2;
      } else if ((token == 'c') && (infixExpression[i + 1] == 'o') &&
                 (infixExpression[i + 2] == 's')) {
        output[j++] = token;
        i += 2;
      } else if ((token == 's') && (infixExpression[i + 1] == 'i') &&
                 (infixExpression[i + 2] == 'n')) {
        output[j++] = token;
        i += 2;
      } else if ((token == 't') && (infixExpression[i + 1] == 'a') &&
                 (infixExpression[i + 2] == 'n')) {
        output[j++] = token;
        i += 2;
      } else if ((token == 'a') && (infixExpression[i + 1] == 'c') &&
                 (infixExpression[i + 2] == 'o') &&
                 (infixExpression[i + 3] == 's')) {
        output[j++] = 'x';
        i += 3;
      } else if ((token == 'a') && (infixExpression[i + 1] == 's') &&
                 (infixExpression[i + 2] == 'i') &&
                 (infixExpression[i + 3] == 'n')) {
        output[j++] = token;
        i += 3;
      } else if ((token == 'a') && (infixExpression[i + 1] == 't') &&
                 (infixExpression[i + 2] == 'a') &&
                 (infixExpression[i + 3] == 'n')) {
        output[j++] = 'r';
        i += 3;
      } else if ((token == 's') && (infixExpression[i + 1] == 'q') &&
                 (infixExpression[i + 2] == 'r') &&
                 (infixExpression[i + 3] == 't')) {
        output[j++] = 'q';
        i += 3;
      } else if ((token == 'l') && (infixExpression[i + 1] == 'n')) {
        output[j++] = 'n';
        i += 1;
      } else if ((token == 'l') && (infixExpression[i + 1] == 'o') &&
                 (infixExpression[i + 2] == 'g')) {
        output[j++] = 'o';
        i += 2;
      } else
        error = 1;
    } else
      error = 1;
  }
  output[strlen(output)] = '\0';
  strcpy(infixExpression, output);
  infixExpression[strlen(output)] = '\0';
  return error;
}

int validator(char *infixExpression) {
  int count = 0, error = 0;
  for (int i = 0; infixExpression[i] != '\0'; ++i) {
    char token = infixExpression[i];
    if (token == '(')
      count++;
    else if (token == ')')
      count--;
    if (isdigit(token)) {
      if (infixExpression[i + 1] == ' ')
        error = 0;
      else if (infixExpression[i + 1] == '.')
        error = 0;
      else if (isdigit(infixExpression[i + 1]))
        error = 0;
      else if (strchr("+-*/^m)", infixExpression[i + 1]) != NULL)
        error = 0;
      else
        error = 1;
    }
    if (error == 1) break;
  }
  if (count != 0) error = 1;
  return error;
}

void changeX(char *infixExpression, char *elem, char *result) {
  char temp[255] = "\0";
  int j = 0;
  if (strchr(infixExpression, 'x') != NULL) {
    for (int i = 0; infixExpression[i] != '\0'; i++) {
      if (infixExpression[i] != 'x') {
        temp[j] = infixExpression[i];
        j++;
      } else {
        temp[j] = '(';
        j++;
        for (unsigned int q = 0; q < strlen(elem); q++) {
          temp[j] = elem[q];
          j++;
        }
        temp[j] = ')';
        j++;
      }
    }
    temp[j] = '\0';
  }
  if (temp != NULL) {
    strcpy(result, temp);
  } else {
    strcpy(result, infixExpression);
  }
}

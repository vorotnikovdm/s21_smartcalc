#include "s21_test_smartcalc.h"

START_TEST(test_sum_smartcalc_normal_1) {
  char *infixExpression = "1.2+x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 + 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sum_smartcalc_normal_2) {
  char *infixExpression = "1.2+x +x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 + 2.5 + 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sum_smartcalc_normal_3) {
  char *infixExpression = "1.2+(-x)";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 + (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sum_smartcalc_normal_4) {
  char *infixExpression = "1.2+x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 + (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sum_smartcalc_normal_5) {
  char *infixExpression = "1.2+x +x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 + (-2.5) + (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sum_smartcalc_normal_6) {
  char *infixExpression = "1.2+(-x)";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 + (-(-2.5));
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_1) {
  char *infixExpression = "1.2-x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 - 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_2) {
  char *infixExpression = "1.2-x -x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 - 2.5 - 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_3) {
  char *infixExpression = "1.2-(-x)";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 - (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_4) {
  char *infixExpression = "1.2-x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 - (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_5) {
  char *infixExpression = "1.2-x -x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 - (-2.5) - (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sub_smartcalc_normal_6) {
  char *infixExpression = "1.2-(-x)";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 - (-(-2.5));
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mul_smartcalc_normal_1) {
  char *infixExpression = "1.2*x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 * 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mul_smartcalc_normal_2) {
  char *infixExpression = "1.2*x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 * (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mul_smartcalc_normal_3) {
  char *infixExpression = "-1.2*x";
  char *x = "-2.5";
  double res = 0.0, resC = -1.2 * -2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mul_smartcalc_normal_4) {
  char *infixExpression = "-1.2*(-x)";
  char *x = "-2.5";
  double res = 0.0, resC = -1.2 * (-(-2.5));
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_div_smartcalc_normal_1) {
  char *infixExpression = "1.2/x";
  char *x = "2.5";
  double res = 0.0, resC = 1.2 / 2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_div_smartcalc_normal_2) {
  char *infixExpression = "1.2/x";
  char *x = "-2.5";
  double res = 0.0, resC = 1.2 / (-2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_div_smartcalc_normal_3) {
  char *infixExpression = "-1.2/x";
  char *x = "-2.5";
  double res = 0.0, resC = -1.2 / -2.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_div_smartcalc_normal_4) {
  char *infixExpression = "-1.2/(-x)";
  char *x = "-2.5";
  double res = 0.0, resC = -1.2 / (-(-2.5));
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_div_smartcalc_normal_5) {
  char *infixExpression = "-1.2/x";
  char *x = "0";
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
  ck_assert(isinf(res) == -1);
}
END_TEST

START_TEST(test_div_smartcalc_normal_6) {
  char *infixExpression = "1.2/x";
  char *x = "0";
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
  ck_assert(isinf(res) == 1);
}
END_TEST

START_TEST(test_pow_smartcalc_normal_1) {
  char *infixExpression = "1.5^x";
  char *x = "2.5";
  double res = 0.0, resC = pow(1.5, 2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_pow_smartcalc_normal_2) {
  char *infixExpression = "1.5^x";
  char *x = "-2.5";
  double res = 0.0, resC = pow(1.5, -2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_pow_smartcalc_normal_3) {
  char *infixExpression = "1.5^(-x)";
  char *x = "-2.5";
  double res = 0.0, resC = pow(1.5, 2.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mod_smartcalc_normal_1) {
  char *infixExpression = "4modx";
  char *x = "2";
  double res = 0.0, resC = fmod(4, 2);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mod_smartcalc_normal_2) {
  char *infixExpression = "4modx";
  char *x = "-1.5";
  double res = 0.0, resC = fmod(4, -1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_mod_smartcalc_normal_3) {
  char *infixExpression = "-14.5modx";
  char *x = "-1.5";
  double res = 0.0, resC = fmod(-14.5, -1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_unary_smartcalc_normal_1) {
  char *infixExpression = "-x";
  char *x = "1.5";
  double res = 0.0, resC = -1.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_unary_smartcalc_normal_2) {
  char *infixExpression = "-x";
  char *x = "-1.5";
  double res = 0.0, resC = 1.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_unary_smartcalc_normal_3) {
  char *infixExpression = "+x";
  char *x = "1.5";
  double res = 0.0, resC = 1.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_unary_smartcalc_normal_4) {
  char *infixExpression = "+x";
  char *x = "-1.5";
  double res = 0.0, resC = -1.5;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sin_smartcalc_normal_1) {
  char *infixExpression = "sin(x)";
  char *x = "-1.5";
  double res = 0.0, resC = sin(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sin_smartcalc_normal_2) {
  char *infixExpression = "sin(-x)";
  char *x = "-1.5";
  double res = 0.0, resC = sin(1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sin_smartcalc_normal_3) {
  char *infixExpression = "-sin(x)";
  char *x = "-1.5";
  double res = 0.0, resC = -sin(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_cos_smartcalc_normal_1) {
  char *infixExpression = "cos(x)";
  char *x = "-1.5";
  double res = 0.0, resC = cos(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_cos_smartcalc_normal_2) {
  char *infixExpression = "cos(-x)";
  char *x = "-1.5";
  double res = 0.0, resC = cos(1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_cos_smartcalc_normal_3) {
  char *infixExpression = "-cos(x)";
  char *x = "-1.5";
  double res = 0.0, resC = -cos(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_tan_smartcalc_normal_1) {
  char *infixExpression = "tan(x)";
  char *x = "-1.5";
  double res = 0.0, resC = tan(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_tan_smartcalc_normal_2) {
  char *infixExpression = "tan(-x)";
  char *x = "-1.5";
  double res = 0.0, resC = tan(1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_tan_smartcalc_normal_3) {
  char *infixExpression = "-tan(x)";
  char *x = "-1.5";
  double res = 0.0, resC = -tan(-1.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_asin_smartcalc_normal_1) {
  char *infixExpression = "asin(x)";
  char *x = "-0.5";
  double res = 0.0, resC = asin(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_asin_smartcalc_normal_2) {
  char *infixExpression = "asin(-x)";
  char *x = "-0.5";
  double res = 0.0, resC = asin(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_asin_smartcalc_normal_3) {
  char *infixExpression = "-asin(x)";
  char *x = "-0.5";
  double res = 0.0, resC = -asin(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_acos_smartcalc_normal_1) {
  char *infixExpression = "acos(x)";
  char *x = "-0.5";
  double res = 0.0, resC = acos(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_acos_smartcalc_normal_2) {
  char *infixExpression = "acos(-x)";
  char *x = "-0.5";
  double res = 0.0, resC = acos(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_acos_smartcalc_normal_3) {
  char *infixExpression = "-acos(x)";
  char *x = "-0.5";
  double res = 0.0, resC = -acos(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_atan_smartcalc_normal_1) {
  char *infixExpression = "atan(x)";
  char *x = "-0.5";
  double res = 0.0, resC = atan(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_atan_smartcalc_normal_2) {
  char *infixExpression = "atan(-x)";
  char *x = "-0.5";
  double res = 0.0, resC = atan(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_atan_smartcalc_normal_3) {
  char *infixExpression = "-atan(x)";
  char *x = "-0.5";
  double res = 0.0, resC = -atan(-0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sqrt_smartcalc_normal_1) {
  char *infixExpression = "sqrt(x)";
  char *x = "10.5";
  double res = 0.0, resC = sqrt(10.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sqrt_smartcalc_normal_2) {
  char *infixExpression = "sqrt(x)";
  char *x = "12";
  double res = 0.0, resC = sqrt(12);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_sqrt_smartcalc_normal_3) {
  char *infixExpression = "-sqrt(x)";
  char *x = "0.5";
  double res = 0.0, resC = -sqrt(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_ln_smartcalc_normal_1) {
  char *infixExpression = "ln(x)";
  char *x = "0.5";
  double res = 0.0, resC = log(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_ln_smartcalc_normal_2) {
  char *infixExpression = "ln(x)";
  char *x = "10.5";
  double res = 0.0, resC = log(10.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_ln_smartcalc_normal_3) {
  char *infixExpression = "-ln(x)";
  char *x = "12";
  double res = 0.0, resC = -log(12);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_log_smartcalc_normal_1) {
  char *infixExpression = "log(x)";
  char *x = "0.5";
  double res = 0.0, resC = log10(0.5);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_log_smartcalc_normal_2) {
  char *infixExpression = "log(x)";
  char *x = "10.43";
  double res = 0.0, resC = log10(10.43);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_log_smartcalc_normal_3) {
  char *infixExpression = "-log(x)";
  char *x = "12";
  double res = 0.0, resC = -log10(12);
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_smartcalc_normal_1) {
  char *infixExpression = "2*5+3";
  char *x = NULL;
  double res = 0.0, resC = 2 * 5 + 3;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_smartcalc_normal_2) {
  char *infixExpression = "((((1.21321312.12.12.12.21.)";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_3) {
  char *infixExpression = "san(x)";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_4) {
  char *infixExpression = "sin(x)+";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_5) {
  char *infixExpression = "++2-2";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_6) {
  char *infixExpression = "+2-+2";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_7) {
  char *infixExpression = "-2.0-(+1.5/(2*sin(x)))";
  char *x = "1.0";
  double res = 0.0, resC = -2.0 - (1.5 / (2 * sin(1.0)));
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, resC, S21_ACCURACY);
}
END_TEST

START_TEST(test_smartcalc_normal_8) {
  char *infixExpression = "";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_9) {
  char *infixExpression = "asdaxqweq";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 1);
}
END_TEST

START_TEST(test_smartcalc_normal_10) {
  char *infixExpression = "1 + 2";
  char *x = NULL;
  double res = 0.0;
  int error = calc_test(infixExpression, x, &res);
  ck_assert(error == 0);
  ck_assert_ldouble_eq_tol(res, 3.0, S21_ACCURACY);
}
END_TEST

START_TEST(test_credit_ann) {
  int loan = 1000;
  int months = 12;
  double interest = 0.1;
  double double_loan = loan;
  double monthInterest = interest / 12;
  double monthly =
      (monthInterest * double_loan) / (1 - pow(1 + monthInterest, -months));
  double interest_paid = 0.0, principal = 0.0, remaining_loan = double_loan,
         total_paid = 0.0, total_interest = 0.0, total_principal = 0.0;
  for (int i = 0; i < months; i++) {
    credit_annuity(monthInterest, monthly, &principal, &interest_paid,
                   &remaining_loan);
    total_paid += monthly;
    total_interest += interest_paid;
    total_principal += principal;
  }
  ck_assert_ldouble_eq_tol(total_paid, 1054.990647, 0.001);
  ck_assert_ldouble_eq_tol(total_interest, 1000.050000, 0.001);
  ck_assert_ldouble_eq_tol(total_principal, 54.990000, 0.001);
}
END_TEST

Suite *s21_test_smartcalc() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_smartcalc_test");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_sum_smartcalc_normal_1);
  tcase_add_test(tc_core, test_sum_smartcalc_normal_2);
  tcase_add_test(tc_core, test_sum_smartcalc_normal_3);
  tcase_add_test(tc_core, test_sum_smartcalc_normal_4);
  tcase_add_test(tc_core, test_sum_smartcalc_normal_5);
  tcase_add_test(tc_core, test_sum_smartcalc_normal_6);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_1);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_2);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_3);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_4);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_5);
  tcase_add_test(tc_core, test_sub_smartcalc_normal_6);
  tcase_add_test(tc_core, test_mul_smartcalc_normal_1);
  tcase_add_test(tc_core, test_mul_smartcalc_normal_2);
  tcase_add_test(tc_core, test_mul_smartcalc_normal_3);
  tcase_add_test(tc_core, test_mul_smartcalc_normal_4);
  tcase_add_test(tc_core, test_div_smartcalc_normal_1);
  tcase_add_test(tc_core, test_div_smartcalc_normal_2);
  tcase_add_test(tc_core, test_div_smartcalc_normal_3);
  tcase_add_test(tc_core, test_div_smartcalc_normal_4);
  tcase_add_test(tc_core, test_div_smartcalc_normal_5);
  tcase_add_test(tc_core, test_div_smartcalc_normal_6);
  tcase_add_test(tc_core, test_pow_smartcalc_normal_1);
  tcase_add_test(tc_core, test_pow_smartcalc_normal_2);
  tcase_add_test(tc_core, test_pow_smartcalc_normal_3);
  tcase_add_test(tc_core, test_mod_smartcalc_normal_1);
  tcase_add_test(tc_core, test_mod_smartcalc_normal_2);
  tcase_add_test(tc_core, test_mod_smartcalc_normal_3);
  tcase_add_test(tc_core, test_unary_smartcalc_normal_1);
  tcase_add_test(tc_core, test_unary_smartcalc_normal_2);
  tcase_add_test(tc_core, test_unary_smartcalc_normal_3);
  tcase_add_test(tc_core, test_unary_smartcalc_normal_4);
  tcase_add_test(tc_core, test_sin_smartcalc_normal_1);
  tcase_add_test(tc_core, test_sin_smartcalc_normal_2);
  tcase_add_test(tc_core, test_sin_smartcalc_normal_3);
  tcase_add_test(tc_core, test_cos_smartcalc_normal_1);
  tcase_add_test(tc_core, test_cos_smartcalc_normal_2);
  tcase_add_test(tc_core, test_cos_smartcalc_normal_3);
  tcase_add_test(tc_core, test_tan_smartcalc_normal_1);
  tcase_add_test(tc_core, test_tan_smartcalc_normal_2);
  tcase_add_test(tc_core, test_tan_smartcalc_normal_3);
  tcase_add_test(tc_core, test_asin_smartcalc_normal_1);
  tcase_add_test(tc_core, test_asin_smartcalc_normal_2);
  tcase_add_test(tc_core, test_asin_smartcalc_normal_3);
  tcase_add_test(tc_core, test_acos_smartcalc_normal_1);
  tcase_add_test(tc_core, test_acos_smartcalc_normal_2);
  tcase_add_test(tc_core, test_acos_smartcalc_normal_3);
  tcase_add_test(tc_core, test_atan_smartcalc_normal_1);
  tcase_add_test(tc_core, test_atan_smartcalc_normal_2);
  tcase_add_test(tc_core, test_atan_smartcalc_normal_3);
  tcase_add_test(tc_core, test_sqrt_smartcalc_normal_1);
  tcase_add_test(tc_core, test_sqrt_smartcalc_normal_2);
  tcase_add_test(tc_core, test_sqrt_smartcalc_normal_3);
  tcase_add_test(tc_core, test_ln_smartcalc_normal_1);
  tcase_add_test(tc_core, test_ln_smartcalc_normal_2);
  tcase_add_test(tc_core, test_ln_smartcalc_normal_3);
  tcase_add_test(tc_core, test_log_smartcalc_normal_1);
  tcase_add_test(tc_core, test_log_smartcalc_normal_2);
  tcase_add_test(tc_core, test_log_smartcalc_normal_3);
  tcase_add_test(tc_core, test_smartcalc_normal_1);
  tcase_add_test(tc_core, test_smartcalc_normal_2);
  tcase_add_test(tc_core, test_smartcalc_normal_3);
  tcase_add_test(tc_core, test_smartcalc_normal_4);
  tcase_add_test(tc_core, test_smartcalc_normal_5);
  tcase_add_test(tc_core, test_smartcalc_normal_6);
  tcase_add_test(tc_core, test_smartcalc_normal_7);
  tcase_add_test(tc_core, test_smartcalc_normal_8);
  tcase_add_test(tc_core, test_smartcalc_normal_9);
  tcase_add_test(tc_core, test_smartcalc_normal_10);
  tcase_add_test(tc_core, test_credit_ann);

  suite_add_tcase(s, tc_core);
  return s;
}

#ifndef S21_TEST_SMARTCALC
#define S21_TEST_SMARTCALC

#include <check.h>

#include "../s21_credit.h"
#include "../s21_smartcalc.h"

#define S21_ACCURACY 0.0000001

Suite *s21_test_smartcalc();

void run_test();
int calc_test(char *infixExpression, char *x, double *res);

#endif

#include "s21_test_smartcalc.h"

int main() {
  run_test();
  return 0;
}

void run_test() {
  Suite *s = NULL;
  SRunner *sr = NULL;

  s = s21_test_smartcalc();
  sr = srunner_create(s);
  srunner_run_all(sr, CK_NORMAL);
  srunner_free(sr);
}

int calc_test(char *infixExpression, char *x, double *res) {
  int error = 0;
  int count = 0;
  char *result = NULL;
  for (unsigned int i = 0; infixExpression[i] != '\0'; i++) {
    if (infixExpression[i] == 'x') {
      count++;
    }
  }
  if ((count > 0) && (x != NULL)) {
    result = malloc((strlen(infixExpression) + (strlen(x) + 2) * count) *
                    sizeof(char));
    changeX(infixExpression, x, result);
  }
  char *fixed;
  if (result != NULL) {
    fixed = malloc((strlen(result) + 1) * sizeof(char));
    strcpy(fixed, result);
    free(result);
  } else {
    fixed = malloc((strlen(infixExpression) + 1) * sizeof(char));
    strcpy(fixed, infixExpression);
  }
  int valid = validator(fixed);
  if (valid == 0) {
    int pars = parser(fixed);
    if (pars == 0) {
      char *postfixExpression = malloc(strlen(fixed) * 2 * sizeof(char));
      int shunt = shuntingYard(fixed, postfixExpression);
      if (shunt == 0) {
        int calc = calcPostfix(postfixExpression, res);
        if (calc == 1) {
          error = 1;
        }
      } else {
        error = 1;
      }
      free(postfixExpression);
    } else
      error = 1;
  } else
    error = 1;
  free(fixed);
  return error;
}

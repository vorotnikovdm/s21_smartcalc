#ifndef S21_SMARTCALC
#define S21_SMARTCALC

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char *array;
  int size;
  int top;
} Stack;

void initialize(Stack *stack, int size);
int isEmpty(Stack *stack);
int push(Stack *stack, char elem);
int pop(Stack *stack, char *elem);
int getPrecedence(char oper);
int shuntingYard(char *infixExpression, char *postfixExpression);
int validator(char *infixExpression);
int parser(char *infixExpression);
int isUnary(char ch);
int postfixCheck(char *postfixExpression);
void changeX(char *infixExpression, char *elem, char *result);

typedef struct {
  double *array;
  int size;
  int top;
} doubleStack;

void doubleinitialize(doubleStack *stack, int size);
int doubleisEmpty(doubleStack *stack);
int doublepush(doubleStack *stack, double elem);
int doublepop(doubleStack *stack, double *elem);
int calcPostfix(char *postfixExpression, double *result);
#endif

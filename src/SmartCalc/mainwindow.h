#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>

extern "C" {
#include "../s21_credit.h"
#include "../s21_smartcalc.h"
}

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private slots:
  void on_pushButton_0_clicked();

  void on_pushButton_1_clicked();

  void on_pushButton_2_clicked();

  void on_pushButton_3_clicked();

  void on_pushButton_4_clicked();

  void on_pushButton_5_clicked();

  void on_pushButton_6_clicked();

  void on_pushButton_7_clicked();

  void on_pushButton_8_clicked();

  void on_pushButton_9_clicked();

  void on_pushButton_acos_clicked();

  void on_pushButton_add_clicked();

  void on_pushButton_asin_clicked();

  void on_pushButton_atan_clicked();

  void on_pushButton_cos_clicked();

  void on_pushButton_div_clicked();

  void on_pushButton_dot_clicked();

  void on_pushButton_eq_clicked();

  void on_pushButton_ln_clicked();

  void on_pushButton_log_clicked();

  void on_pushButton_mod_clicked();

  void on_pushButton_mul_clicked();

  void on_pushButton_parend_clicked();

  void on_pushButton_parstart_clicked();

  void on_pushButton_pow_clicked();

  void on_pushButton_sin_clicked();

  void on_pushButton_sqrt_clicked();

  void on_pushButton_sub_clicked();

  void on_pushButton_tan_clicked();

  void on_pushButton_x_clicked();

  void on_pushButton_c_clicked();

  void on_pushButton_graph_clicked();

  void on_pushButton_clicked();

  void on_checkBox_toggled(bool checked);

  void on_pushButton_10_clicked();

  void on_pushButton_c_2_clicked();

 private:
  Ui::MainWindow *ui;

  double calcFunction(double x);
  void annCredit(int loan, int months, double interest);
  void annTable(int i, double monthly, double interest_paid, double principal,
                double remaining_loan);
  void annTableTotal(int months, double total_paid, double total_interest,
                     double total_principal);
  void diffCredit(int loan, int months, double interest);
};
#endif  // MAINWINDOW_H

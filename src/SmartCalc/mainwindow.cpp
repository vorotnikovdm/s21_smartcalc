#include "mainwindow.h"

#include <cmath>

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->label_18->setVisible(false);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pushButton_0_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "0");
}

void MainWindow::on_pushButton_1_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "1");
}

void MainWindow::on_pushButton_2_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "2");
}

void MainWindow::on_pushButton_3_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "3");
}

void MainWindow::on_pushButton_4_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "4");
}

void MainWindow::on_pushButton_5_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "5");
}

void MainWindow::on_pushButton_6_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "6");
}

void MainWindow::on_pushButton_7_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "7");
}

void MainWindow::on_pushButton_8_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "8");
}

void MainWindow::on_pushButton_9_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "9");
}

void MainWindow::on_pushButton_acos_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "acos");
}

void MainWindow::on_pushButton_add_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "+");
}

void MainWindow::on_pushButton_asin_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "asin");
}

void MainWindow::on_pushButton_atan_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "atan");
}

void MainWindow::on_pushButton_cos_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "cos");
}

void MainWindow::on_pushButton_div_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "/");
}

void MainWindow::on_pushButton_dot_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + ".");
}

void MainWindow::on_pushButton_eq_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);
  QString infixExpression = ui->lineEdit->text();
  char *infixExpressionCstr = qstrdup(infixExpression.toUtf8().constData());
  QString xString = ui->lineEdit_3->text();
  char *xStringCstr = qstrdup(xString.toUtf8().constData());
  int count = 0;
  for (unsigned int i = 0; infixExpressionCstr[i] != '\0'; i++) {
    if (infixExpressionCstr[i] == 'x') count++;
  }
  if (count > 0) {
    char *res;
    res = (char *)malloc(
        (strlen(infixExpressionCstr) + (strlen(xStringCstr) + 2) * count) *
        sizeof(char));
    changeX(infixExpressionCstr, xStringCstr, res);
    free(infixExpressionCstr);
    infixExpressionCstr = (char *)malloc((strlen(res) + 1) * sizeof(char));
    strcpy(infixExpressionCstr, res);
  }
  int valid = validator(infixExpressionCstr);
  if (valid == 0) {
    int error = parser(infixExpressionCstr);
    if (error == 0) {
      char *postfixExpression = static_cast<char *>(
          malloc(strlen(infixExpressionCstr) * 2 * sizeof(char)));
      int shunt = shuntingYard(infixExpressionCstr, postfixExpression);
      if (shunt == 0) {
        double res = 0.0;
        int calc = calcPostfix(postfixExpression, &res);
        if (calc == 0) {
          ui->lineEdit->setText(QString::number(res));
          free(postfixExpression);
        } else {
          ui->lineEdit->setText("nan");
          free(postfixExpression);
        }
      } else {
        ui->lineEdit->setText("nan");
        free(postfixExpression);
      }
    } else
      ui->lineEdit->setText("nan");
  } else
    ui->lineEdit->setText("nan");
  delete[] infixExpressionCstr;
  delete[] xStringCstr;
}

void MainWindow::on_pushButton_ln_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "ln");
}

void MainWindow::on_pushButton_log_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "log");
}

void MainWindow::on_pushButton_mod_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "mod");
}

void MainWindow::on_pushButton_mul_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "*");
}

void MainWindow::on_pushButton_parend_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + ")");
}

void MainWindow::on_pushButton_parstart_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "(");
}

void MainWindow::on_pushButton_pow_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "^");
}

void MainWindow::on_pushButton_sin_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "sin");
}

void MainWindow::on_pushButton_sqrt_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "sqrt");
}

void MainWindow::on_pushButton_sub_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "-");
}

void MainWindow::on_pushButton_tan_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "tan");
}

void MainWindow::on_pushButton_x_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "x");
}

void MainWindow::on_pushButton_c_clicked() { ui->lineEdit->setText(""); }

void MainWindow::on_pushButton_graph_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  QString xminText = ui->lineEdit_xmin->text();
  bool convXmin;
  double xmin = xminText.toDouble(&convXmin);

  QString xmaxText = ui->lineEdit_xmax->text();
  bool convXmax;
  double xmax = xmaxText.toDouble(&convXmax);

  QString yminText = ui->lineEdit_ymin->text();
  bool convYmin;
  double ymin = yminText.toDouble(&convYmin);

  QString ymaxText = ui->lineEdit_ymax->text();
  bool convYmax;
  double ymax = ymaxText.toDouble(&convYmax);

  if (convXmin && convXmax && convYmin && convYmax) {
    ui->widget->xAxis->setRange(xmin, xmax);
    ui->widget->yAxis->setRange(ymin, ymax);
    QVector<double> xData, yData;
    double shag = (xmax - xmin) / 100;
    for (double x = xmin; x < xmax; x += shag) {
      double y = calcFunction(x);
      if (std::isfinite(y) && !std::isnan(y)) {
        xData.push_back(x);
        yData.push_back(y);
      }
    }
    ui->widget->addGraph();
    ui->widget->graph(0)->setPen(QPen(Qt::blue));
    ui->widget->graph(0)->setData(xData, yData);
    ui->widget->replot();
  } else {
    ui->widget->removeGraph(0);
    ui->widget->replot();
  }
}

double MainWindow::calcFunction(double x) {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);
  double result = 0.0;
  QString infixExpression = ui->lineEdit->text();
  char *infixExpressionCstr = qstrdup(infixExpression.toUtf8().constData());
  QString xString = QString::number(x);
  char *xStringCstr = qstrdup(xString.toUtf8().constData());
  int count = 0;
  for (unsigned int i = 0; infixExpressionCstr[i] != '\0'; i++) {
    if (infixExpressionCstr[i] == 'x') count++;
  }
  if ((count > 0) && (xStringCstr != NULL)) {
    char *res;
    res = (char *)malloc(
        (strlen(infixExpressionCstr) + (strlen(xStringCstr) + 2) * count) *
        sizeof(char));
    changeX(infixExpressionCstr, xStringCstr, res);
    free(infixExpressionCstr);
    infixExpressionCstr = (char *)malloc((strlen(res) + 1) * sizeof(char));
    strcpy(infixExpressionCstr, res);
  }
  int valid = validator(infixExpressionCstr);
  if (valid == 0) {
    int error = parser(infixExpressionCstr);
    if (error == 0) {
      char *postfixExpression = static_cast<char *>(
          malloc(strlen(infixExpressionCstr) * 2 * sizeof(char)));
      int shunt = shuntingYard(infixExpressionCstr, postfixExpression);
      if (shunt == 0) {
        calcPostfix(postfixExpression, &result);
      }
    }
  }
  return result;
}

void MainWindow::on_pushButton_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  ui->tableWidget->clear();
  ui->tableWidget->setRowCount(0);
  ui->tableWidget->setColumnCount(5);

  QString loanText = ui->lineEdit_loan->text();
  bool convLoan;
  double loan = loanText.toDouble(&convLoan);

  QString monthsText = ui->lineEdit_months->text();
  bool convMonths;
  int months = monthsText.toInt(&convMonths);

  QString interestText = ui->lineEdit_interest->text();
  bool convInterest;
  double interest = interestText.toDouble(&convInterest);
  interest = (interest / 100);
  if (convLoan && convMonths && convInterest && loan > 0 && months > 0 &&
      interest > 0) {
    int comboBox_2 = ui->comboBox_2->currentIndex();
    if (comboBox_2 == 1) months *= 12;
    ui->tableWidget->setRowCount(months + 2);
    QTableWidgetItem *item = new QTableWidgetItem("№");
    ui->tableWidget->setItem(0, 0, item);
    item = new QTableWidgetItem("Сумма");
    ui->tableWidget->setItem(0, 1, item);
    item = new QTableWidgetItem("Основной");
    ui->tableWidget->setItem(0, 2, item);
    item = new QTableWidgetItem("Проценты");
    ui->tableWidget->setItem(0, 3, item);
    item = new QTableWidgetItem("Остаток");
    ui->tableWidget->setItem(0, 4, item);
    int comboBox_1 = ui->comboBox->currentIndex();
    if (comboBox_1 == 0) {
      annCredit(loan, months, interest);
    } else
      diffCredit(loan, months, interest);
  }
}

void MainWindow::annCredit(int loan, int months, double interest) {
  double double_loan = loan;
  double monthInterest = interest / 12;
  double monthly =
      (monthInterest * double_loan) / (1 - pow(1 + monthInterest, -months));
  double interest_paid = 0.0, principal = 0.0, remaining_loan = double_loan,
         total_paid = 0.0, total_interest = 0.0, total_principal = 0.0;
  for (int i = 0; i < months; i++) {
    credit_annuity(monthInterest, monthly, &principal, &interest_paid,
                   &remaining_loan);
    annTable(i, monthly, interest_paid, principal, remaining_loan);
    total_paid += monthly;
    total_interest += interest_paid;
    total_principal += principal;
  }
  annTableTotal(months, total_paid, total_interest, total_principal);
}

void MainWindow::annTable(int i, double monthly, double interest_paid,
                          double principal, double remaining_loan) {
  monthly = round(monthly * 100) / 100;
  interest_paid = round(interest_paid * 100) / 100;
  principal = round(principal * 100) / 100;
  remaining_loan = round(remaining_loan * 100) / 100;
  QString iTemp = QString::number(i + 1);
  QString monthlyTemp = QString::number(monthly);
  QString interest_paidTemp = QString::number(interest_paid);
  QString principalTemp = QString::number(principal);
  QString remaining_loanTemp = QString::number(remaining_loan);
  QTableWidgetItem *item = new QTableWidgetItem(iTemp);
  ui->tableWidget->setItem(i + 1, 0, item);
  item = new QTableWidgetItem(monthlyTemp);
  ui->tableWidget->setItem(i + 1, 1, item);
  item = new QTableWidgetItem(interest_paidTemp);
  ui->tableWidget->setItem(i + 1, 2, item);
  item = new QTableWidgetItem(principalTemp);
  ui->tableWidget->setItem(i + 1, 3, item);
  item = new QTableWidgetItem(remaining_loanTemp);
  ui->tableWidget->setItem(i + 1, 4, item);
}

void MainWindow::annTableTotal(int months, double total_paid,
                               double total_interest, double total_principal) {
  total_paid = round(total_paid * 100) / 100;
  total_interest = round(total_interest * 100) / 100;
  total_principal = round(total_principal * 100) / 100;
  QString paidTemp = QString::number(total_paid);
  QString interestTemp = QString::number(total_interest);
  QString principalTemp = QString::number(total_principal);
  QTableWidgetItem *item = new QTableWidgetItem(paidTemp);
  ui->tableWidget->setItem(months + 1, 1, item);
  item = new QTableWidgetItem(interestTemp);
  ui->tableWidget->setItem(months + 1, 2, item);
  item = new QTableWidgetItem(principalTemp);
  ui->tableWidget->setItem(months + 1, 3, item);
  item = new QTableWidgetItem("Total:");
  ui->tableWidget->setItem(months + 1, 0, item);
}

void MainWindow::diffCredit(int loan, int months, double interest) {
  double loan_double = loan;
  double monthly = loan_double / months;
  double monthInterest = 0.0, principal = 0.0, total_paid = 0.0,
         total_monthly = 0.0, total_interest = 0.0;
  for (int i = 0; i < months; i++) {
    monthInterest = loan_double * (interest / 12);
    loan_double -= monthly;
    principal = monthly + monthInterest;
    annTable(i, principal, monthly, monthInterest, loan_double);
    total_paid += principal;
    total_monthly += monthly;
    total_interest += monthInterest;
  }
  annTableTotal(months, total_paid, total_monthly, total_interest);
}

void MainWindow::on_checkBox_toggled(bool checked) {
  if (checked) {
    ui->label_17->setEnabled(false);
    ui->label_17->setVisible(false);
    ui->label_18->setEnabled(true);
    ui->label_18->setVisible(true);
  } else {
    ui->label_17->setEnabled(true);
    ui->label_17->setVisible(true);
    ui->label_18->setEnabled(false);
    ui->label_18->setVisible(false);
  }
}

void MainWindow::on_pushButton_10_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  ui->tableWidget_2->clear();
  ui->tableWidget_2->setRowCount(0);
  ui->tableWidget_2->setColumnCount(5);

  QString depositText = ui->lineEdit_deposit->text();
  bool convDeposit;
  double deposit = depositText.toDouble(&convDeposit);

  QString monthsText = ui->lineEdit_months_2->text();
  bool convMonths;
  double months = monthsText.toDouble(&convMonths);

  QString interestText = ui->lineEdit_interest_2->text();
  bool convInterest;
  double interest = interestText.toDouble(&convInterest);

  QString taxText = ui->lineEdit_tax->text();
  bool convTax;
  double tax = taxText.toDouble(&convTax);

  if (convDeposit && convMonths && convInterest && convTax && deposit > 0 &&
      months > 0 && interest > 0 && tax > 0) {
    int comboBox_3 = ui->comboBox_3->currentIndex();
    int comboBox_4 = ui->comboBox_4->currentIndex();
    if (comboBox_3 == 1) months *= 12;
    if (comboBox_4 == 0)
      ui->tableWidget_2->setRowCount(months + 4);
    else {
      int rowscount = months / 12;
      if (rowscount * 12 - months != 0) rowscount++;
      ui->tableWidget_2->setRowCount(rowscount + 4);
    }
    QTableWidgetItem *item = new QTableWidgetItem("№");
    ui->tableWidget_2->setItem(0, 0, item);
    item = new QTableWidgetItem("Проценты");
    ui->tableWidget_2->setItem(0, 1, item);
    item = new QTableWidgetItem("Пополнение");
    ui->tableWidget_2->setItem(0, 2, item);
    item = new QTableWidgetItem("Выплата");
    ui->tableWidget_2->setItem(0, 3, item);
    item = new QTableWidgetItem("Баланс");
    ui->tableWidget_2->setItem(0, 4, item);
    item = new QTableWidgetItem("+" + depositText);
    ui->tableWidget_2->setItem(1, 2, item);
    item = new QTableWidgetItem(depositText);
    ui->tableWidget_2->setItem(1, 4, item);
    double start_deposit = deposit;
    double temp_interest = 0.0;
    double temp = 0.0;
    temp = 1 + interest / 100 / 12;
    int check = ui->checkBox->isChecked();
    double tax_check = 0.0, tax_total = 0.0, tax_min = 1000000 * 16 / 100;
    if (check == 1) {
      int row = 0;
      int temp_count = 0;
      for (int i = 0; i < months; i++) {
        temp_interest = deposit;
        temp_count++;
        if (comboBox_4 == 0)
          deposit *= temp;
        else if (((comboBox_4 == 1) && (temp_count % 12 == 0)) ||
                 ((comboBox_4 == 1) && (i == months - 1))) {
          deposit *= temp;

          deposit = (deposit - temp_interest) * temp_count + temp_interest;
        }
        tax_check += deposit - temp_interest;
        if ((comboBox_4 == 0) ||
            ((comboBox_4 == 1) && (temp_count % 12 == 0)) ||
            ((comboBox_4 == 1) && (i == months - 1))) {
          QString rowTemp = QString::number(row + 1);
          item = new QTableWidgetItem(rowTemp);
          ui->tableWidget_2->setItem(row + 2, 0, item);
          QString interestTemp = QString::number(deposit - temp_interest);
          item = new QTableWidgetItem(interestTemp);
          ui->tableWidget_2->setItem(row + 2, 1, item);
          QString interestTemp_2 = QString::number(deposit - temp_interest);
          item = new QTableWidgetItem("+" + interestTemp_2);
          ui->tableWidget_2->setItem(row + 2, 2, item);
          QString depositTemp = QString::number(deposit);
          item = new QTableWidgetItem(depositTemp);
          ui->tableWidget_2->setItem(row + 2, 4, item);
          row++;
        }
        if ((temp_count % 12 == 0) || (i == months - 1)) {
          if (tax_check > tax_min) {
            tax_total += (tax_check - tax_min) * tax / 100;
          }
          tax_check = 0.0;
        }
        if (temp_count == 12) temp_count = 0;
      }
      item = new QTableWidgetItem("Total:");
      ui->tableWidget_2->setItem(row + 2, 0, item);
      QString totalInterest = QString::number(deposit - start_deposit);
      item = new QTableWidgetItem(totalInterest);
      ui->tableWidget_2->setItem(row + 2, 1, item);
      QString totalDeposit = QString::number(deposit);
      item = new QTableWidgetItem(totalDeposit);
      ui->tableWidget_2->setItem(row + 2, 4, item);
      item = new QTableWidgetItem("Tax:");
      ui->tableWidget_2->setItem(row + 3, 0, item);
      QString taxStr = QString::number(tax_total);
      item = new QTableWidgetItem(taxStr);
      ui->tableWidget_2->setItem(row + 3, 1, item);
    } else {
      double amount = deposit * (temp - 1);
      double sum = 0.0, total = 0.0;
      int count = 0;
      int row = 0;
      for (int i = 0; i < months; i++) {
        sum += amount;
        total += amount;
        tax_check += amount;
        count++;
        if ((comboBox_4 == 0) || ((count % 12 == 0) && (comboBox_4 == 1)) ||
            ((comboBox_4 == 1) && (i == months - 1))) {
          QString rowTemp = QString::number(row + 1);
          item = new QTableWidgetItem(rowTemp);
          ui->tableWidget_2->setItem(row + 2, 0, item);
          QString sumTemp = QString::number(sum);
          item = new QTableWidgetItem(sumTemp);
          ui->tableWidget_2->setItem(row + 2, 1, item);
          QString sumTemp_2 = QString::number(sum);
          item = new QTableWidgetItem(sumTemp_2);
          ui->tableWidget_2->setItem(row + 2, 3, item);
          item = new QTableWidgetItem(depositText);
          ui->tableWidget_2->setItem(row + 2, 4, item);
          sum = 0.0;
          row++;
        }
        if ((count % 12 == 0) || (i == months - 1)) {
          if (tax_check > tax_min) {
            tax_total += (tax_check - tax_min) * tax / 100;
          }
          tax_check = 0.0;
        }
      }
      item = new QTableWidgetItem("Total:");
      ui->tableWidget_2->setItem(row + 2, 0, item);
      QString totalTemp = QString::number(total);
      item = new QTableWidgetItem(totalTemp);
      ui->tableWidget_2->setItem(row + 2, 1, item);
      item = new QTableWidgetItem(depositText);
      ui->tableWidget_2->setItem(row + 2, 4, item);
      item = new QTableWidgetItem("Tax:");
      ui->tableWidget_2->setItem(row + 3, 0, item);
      QString taxStr = QString::number(tax_total);
      item = new QTableWidgetItem(taxStr);
      ui->tableWidget_2->setItem(row + 3, 1, item);
    }
  }
}

void MainWindow::on_pushButton_c_2_clicked() { ui->lineEdit_3->setText(""); }

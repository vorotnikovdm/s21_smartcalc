#ifndef S21_CREDIT
#define S21_CREDIT

#include <math.h>
#include <stdio.h>

void credit_annuity(double interest, double monthly, double *principal,
                    double *interest_paid, double *remaining_loan);
#endif

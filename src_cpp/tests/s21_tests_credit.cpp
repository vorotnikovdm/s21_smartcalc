#include "s21_tests.h"

TEST(Credit, annuity) {
  s21::model_credit temp;
  temp.credit_annuity(15, 15000, 15);

  EXPECT_EQ(temp.getTotalPaid() - 16543.5 < 0.1, 1);
  EXPECT_EQ(temp.getTotalInterest() - 1543.45 < 0.1, 1);
  EXPECT_EQ(temp.getTotalPrincipal() - 15000 < 0.1, 1);
}

TEST(Credit, diff) {
  s21::model_credit temp;
  temp.credit_diff(15, 15000, 15);

  EXPECT_EQ(temp.getTotalPaid() - 16500.0 < 0.1, 1);
  EXPECT_EQ(temp.getTotalInterest() - 1500.0 < 0.1, 1);
  EXPECT_EQ(temp.getTotalPrincipal() - 15000.0 < 0.1, 1);
}

TEST(Credit, calcs) {
  s21::model_credit temp;
  temp.credit_diff(15, 15000, 15);

  EXPECT_EQ(temp.getMonthly(5) - 1125.0 < 0.1, 1);
  EXPECT_EQ(temp.getInterestPaid(5) - 125.0 < 0.1, 1);
  EXPECT_EQ(temp.getPrincipal(5) - 1000.0 < 0.1, 1);
  EXPECT_EQ(temp.getRemainingLoan(5) - 10000.0 < 0.1, 1);
}

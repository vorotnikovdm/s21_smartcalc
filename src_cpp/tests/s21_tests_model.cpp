#include "s21_tests.h"

TEST(Model, setX) {
  s21::model temp;
  temp.setX(3.3);

  EXPECT_EQ(temp.getX(), 3.3);
}

TEST(Model, setExpression) {
  s21::model temp;
  std::string test = "1 + x / cos(x)";
  temp.setExpression(test);

  EXPECT_EQ(test == temp.getExpression(), 1);
}

TEST(Model, changeX) {
  s21::model temp;
  temp.setX(3.3);
  temp.setExpression("1 + x + x");
  temp.changeX();

  EXPECT_EQ(temp.getExpression() == "1 + (3.300000) + (3.300000)", 1);
}

TEST(Model, validator) {
  s21::model temp;
  temp.setExpression("1 + 1");
  temp.validator();

  EXPECT_EQ(temp.getExpression() == "1 + 1", 1);
}

TEST(Model, validator_2) {
  s21::model temp;
  temp.setExpression("((13 + 2.3)");

  EXPECT_THROW(temp.validator(), std::invalid_argument);
}

TEST(Model, validator_3) {
  s21::model temp;
  temp.setExpression("1,4 + 2;;:;21");

  EXPECT_THROW(temp.validator(), std::invalid_argument);
}

TEST(Model, parser) {
  s21::model temp;
  temp.setExpression("sadsawqeq12032,.,dfrlsdl");

  EXPECT_THROW(temp.parser(), std::invalid_argument);
}

TEST(Model, parser_2) {
  s21::model temp;
  temp.setExpression("+1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "k1", 1);
}

TEST(Model, parser_3) {
  s21::model temp;
  temp.setExpression("-1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "l1", 1);
}

TEST(Model, parser_4) {
  s21::model temp;
  temp.setExpression("2mod4");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "2m4", 1);
}

TEST(Model, parser_5) {
  s21::model temp;
  temp.setExpression("cos1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "c1", 1);
}

TEST(Model, parser_6) {
  s21::model temp;
  temp.setExpression("sin1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "s1", 1);
}

TEST(Model, parser_7) {
  s21::model temp;
  temp.setExpression("tan1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "t1", 1);
}

TEST(Model, parser_8) {
  s21::model temp;
  temp.setExpression("acos1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "x1", 1);
}

TEST(Model, parser_9) {
  s21::model temp;
  temp.setExpression("asin1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "a1", 1);
}

TEST(Model, parser_10) {
  s21::model temp;
  temp.setExpression("atan1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "r1", 1);
}

TEST(Model, parser_11) {
  s21::model temp;
  temp.setExpression("sqrt1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "q1", 1);
}

TEST(Model, parser_12) {
  s21::model temp;
  temp.setExpression("ln1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "n1", 1);
}

TEST(Model, parser_13) {
  s21::model temp;
  temp.setExpression("log1");
  temp.parser();

  EXPECT_EQ(temp.getExpression() == "o1", 1);
}

TEST(Model, shuntingYard) {
  s21::model temp;
  temp.setExpression("-(1 + cosx) / 2");
  temp.setX(3.3);
  temp.changeX();
  temp.parser();
  temp.shuntingYard();

  EXPECT_EQ(temp.getExpression() == "1 3.300000 c + 2 / l ", 1);
}

TEST(Model, shuntingYard_2) {
  s21::model temp;
  temp.setExpression("1. + (3.300000) + (3...300000).");

  EXPECT_THROW(temp.shuntingYard(), std::invalid_argument);
}

TEST(Model, getPrecedence) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('+'), 1);
}

TEST(Model, getPrecedence_2) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('-'), 1);
}

TEST(Model, getPrecedence_3) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('k'), 1);
}

TEST(Model, getPrecedence_4) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('l'), 1);
}

TEST(Model, getPrecedence_5) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('*'), 2);
}

TEST(Model, getPrecedence_6) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('/'), 2);
}

TEST(Model, getPrecedence_7) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('m'), 2);
}

TEST(Model, getPrecedence_8) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('^'), 3);
}

TEST(Model, getPrecedence_9) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('s'), 4);
}

TEST(Model, getPrecedence_10) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('c'), 4);
}

TEST(Model, getPrecedence_11) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('t'), 4);
}

TEST(Model, getPrecedence_12) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('a'), 4);
}

TEST(Model, getPrecedence_13) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('x'), 4);
}

TEST(Model, getPrecedence_14) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('r'), 4);
}

TEST(Model, getPrecedence_15) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('n'), 5);
}

TEST(Model, getPrecedence_16) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('o'), 5);
}

TEST(Model, getPrecedence_17) {
  s21::model temp;

  EXPECT_EQ(temp.getPrecedence('q'), 6);
}

TEST(Model, postfixCheck) {
  s21::model temp;

  EXPECT_EQ(temp.postfixCheck("1"), 0);
}

TEST(Model, postfixCheck_2) {
  s21::model temp;

  EXPECT_EQ(temp.postfixCheck("1 + 2.2...1...-"), 1);
}

TEST(Model, postfixCheck_3) {
  s21::model temp;

  EXPECT_EQ(temp.postfixCheck("1 + ."), 1);
}

TEST(Model, calcPostfix) {
  s21::model temp;
  temp.setExpression(
      "(-1) + (+1) - 1 + 1 - 1 * 1 / 1 + 1 ^ 1 + 1mod1 + sin1 + cos1 + tan1 + "
      "asin1 + acos1 + atan1 + sqrt4 + log5 + ln7");
  temp.parser();
  temp.shuntingYard();
  temp.calcPostfix();

  EXPECT_EQ(temp.result() - 9.94026 < 0.00001, 1);
}

TEST(Model, calcPostfix_2) {
  s21::model temp;
  temp.setExpression("1.15 + x");
  temp.setX(3.3);
  temp.changeX();
  temp.parser();
  temp.shuntingYard();
  temp.calcPostfix();

  EXPECT_EQ(temp.result() - 4.45 < 0.00001, 1);
}

TEST(Model, calcPostfix_3) {
  s21::model temp;
  temp.setExpression("2.15 3 4 51 + -");

  EXPECT_THROW(temp.calcPostfix(), std::invalid_argument);
}

TEST(Model, calcPostfix_4) {
  s21::model temp;
  temp.setExpression("5.15 / 0");
  temp.parser();
  temp.shuntingYard();

  EXPECT_THROW(temp.calcPostfix(), std::invalid_argument);
}

TEST(Model, calcPostfix_5) {
  s21::model temp;
  temp.setExpression("3.14 2.71 +");
  temp.calcPostfix();

  EXPECT_EQ(temp.result() - 5.85 < 0.1, 1);
}

TEST(Model, calcPostfix_default) {
  s21::model temp;
  temp.setExpression("5 2 &");

  EXPECT_THROW(temp.calcPostfix(), std::invalid_argument);
}

TEST(Controller, Good) {
  s21::controller temp;

  EXPECT_EQ(
      temp.getResult("(-1) + (+1) - 1 + 1 - 1 * 1 / 1 + 1 ^ 1 + 1mod1 + sin1 + "
                     "cos1 + tan1 + asin1 + acos1 + atan1 + sqrt4 + log5 + ln7",
                     0.0) -
              9.94026 <
          0.00001,
      1);
}

TEST(Controller, Bad) {
  s21::controller temp;

  EXPECT_THROW(temp.getResult("2139120o kzsf;.;.,", 0.0),
               std::invalid_argument);
}

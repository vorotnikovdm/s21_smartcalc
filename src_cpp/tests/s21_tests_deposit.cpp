#include "s21_tests.h"

TEST(Deposit, no_cap_no_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(0.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 0, 1);

  EXPECT_EQ(temp.getTotalInterest() - 2812.5 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 15000 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, no_cap_positive_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 0, 1);

  EXPECT_EQ(temp.getTotalInterest() - 2943.75 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 16500 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, no_cap_negative_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(-100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 0, 1);

  EXPECT_EQ(temp.getTotalInterest() - 2681.25 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 13500 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 1_cap_no_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(0.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 1, 1);

  EXPECT_EQ(temp.getTotalInterest() - 3072.44 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 18072.4 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 1_cap_positive_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 1, 1);

  EXPECT_EQ(temp.getTotalInterest() - 3211.07 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 19711.1 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 1_cap_negative_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(-100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 1, 1);

  EXPECT_EQ(temp.getTotalInterest() - 2933.8 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 16433.8 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 6_cap_no_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(0.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 6, 6);

  EXPECT_EQ(temp.getTotalInterest() - 2984.41 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 17984.4 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 6_cap_positive_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 6, 6);

  EXPECT_EQ(temp.getTotalInterest() - 3120.22 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 19620.2 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, 6_cap_negative_transfer) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(-100.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 15000, 15, 16, 6, 6);

  EXPECT_EQ(temp.getTotalInterest() - 2848.61 < 0.1, 1);
  EXPECT_EQ(temp.getTotalDeposit() - 16348.6 < 0.1, 1);
  EXPECT_EQ(temp.getTotalTax() - 0 < 0.1, 1);
}

TEST(Deposit, bad) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(-5000.0);
  }
  temp.set_transfers(transfers);

  EXPECT_THROW(temp.calc_deposit(15, 15000, 15, 16, 6, 6),
               std::invalid_argument);
}

TEST(Deposit, tax) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(50000.0);
  }
  temp.set_transfers(transfers);
  temp.calc_deposit(15, 1500000, 15, 16, 6, 6);

  EXPECT_EQ(temp.getTotalTax() - 18462.5 < 0.1, 1);
}

TEST(Deposit, get_vector) {
  s21::model_deposit temp;
  std::vector<double> transfers;
  for (int i = 0; i < 15; ++i) {
    transfers.push_back(500.0);
  }
  temp.set_transfers(transfers);

  temp.calc_deposit(15, 15000, 15, 16, 6, 6);
  std::vector<double> interest = temp.getInterestPaid();
  EXPECT_EQ(interest.back() - 909.521 < 0.1, 1);

  std::vector<double> transfers_2 = temp.getTransfers();
  EXPECT_EQ(transfers_2.back() - 500.0 < 0.1, 1);

  std::vector<double> current = temp.getCurrentDeposit();
  EXPECT_EQ(current.back() - 26163.4 < 0.1, 1);
}

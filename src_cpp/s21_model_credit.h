#ifndef S21_MODEL_CREDIT_H
#define S21_MODEL_CREDIT_H

#include <cmath>
#include <iostream>
#include <vector>

namespace s21 {
class model_credit {
 public:
  model_credit() : total_paid(0), total_interest(0), total_principal(0) {}
  ~model_credit() = default;

  void credit_annuity(int months, double loan, double interest);
  void credit_diff(int months, double loan, double interest);

  double getMonthly(int index) const;
  double getInterestPaid(int index) const;
  double getPrincipal(int index) const;
  double getRemainingLoan(int index) const;
  double getTotalPaid() const;
  double getTotalInterest() const;
  double getTotalPrincipal() const;

 private:
  std::vector<double> monthly;
  std::vector<double> interest_paid;
  std::vector<double> principal;
  std::vector<double> remaining_loan;
  double total_paid;
  double total_interest;
  double total_principal;
};
}  // namespace s21

#endif

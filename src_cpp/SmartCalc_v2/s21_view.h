#ifndef S21_VIEW_H
#define S21_VIEW_H

#include <QMainWindow>

#include "../s21_controller.h"
#include "../s21_model.h"
#include "../s21_model_credit.h"
#include "../s21_model_deposit.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class view;
}
QT_END_NAMESPACE

class view : public QMainWindow {
  Q_OBJECT

 public:
  view(QWidget *parent = nullptr);
  ~view();

 private slots:
  void on_pushButton_c_clicked();

  void on_pushButton_c_2_clicked();

  void on_pushButton_0_clicked();

  void on_pushButton_1_clicked();

  void on_pushButton_2_clicked();

  void on_pushButton_3_clicked();

  void on_pushButton_4_clicked();

  void on_pushButton_5_clicked();

  void on_pushButton_6_clicked();

  void on_pushButton_7_clicked();

  void on_pushButton_8_clicked();

  void on_pushButton_9_clicked();

  void on_pushButton_dot_clicked();

  void on_pushButton_add_clicked();

  void on_pushButton_sub_clicked();

  void on_pushButton_mul_clicked();

  void on_pushButton_div_clicked();

  void on_pushButton_parend_clicked();

  void on_pushButton_pow_clicked();

  void on_pushButton_parstart_clicked();

  void on_pushButton_mod_clicked();

  void on_pushButton_x_clicked();

  void on_pushButton_acos_clicked();

  void on_pushButton_cos_clicked();

  void on_pushButton_asin_clicked();

  void on_pushButton_sin_clicked();

  void on_pushButton_atan_clicked();

  void on_pushButton_tan_clicked();

  void on_pushButton_sqrt_clicked();

  void on_pushButton_ln_clicked();

  void on_pushButton_log_clicked();

  void on_pushButton_eq_clicked();

  void on_pushButton_graph_clicked();

  void on_pushButton_clicked();

  void on_checkBox_toggled(bool checked);

  void on_checkBox_2_toggled(bool checked);

  void on_pushButton_10_clicked();

 private:
  Ui::view *ui;

  void fillTable(const s21::model_credit &credit, int months);
  void fillTable_deposit(s21::model_deposit &deposit, int months,
                         int frequency);
};
#endif  // S21_VIEW_H

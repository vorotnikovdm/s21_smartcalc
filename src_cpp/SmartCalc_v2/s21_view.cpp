#include "s21_view.h"

#include "ui_s21_view.h"

view::view(QWidget* parent) : QMainWindow(parent), ui(new Ui::view) {
  ui->setupUi(this);
  ui->label_18->setVisible(false);
}

view::~view() { delete ui; }

void view::on_pushButton_c_clicked() { ui->lineEdit->setText(""); }

void view::on_pushButton_c_2_clicked() { ui->lineEdit_3->setText(""); }

void view::on_pushButton_0_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "0");
}

void view::on_pushButton_1_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "1");
}

void view::on_pushButton_2_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "2");
}

void view::on_pushButton_3_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "3");
}

void view::on_pushButton_4_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "4");
}

void view::on_pushButton_5_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "5");
}

void view::on_pushButton_6_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "6");
}

void view::on_pushButton_7_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "7");
}

void view::on_pushButton_8_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "8");
}

void view::on_pushButton_9_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "9");
}

void view::on_pushButton_dot_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + ".");
}

void view::on_pushButton_add_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "+");
}

void view::on_pushButton_sub_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "-");
}

void view::on_pushButton_mul_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "*");
}

void view::on_pushButton_div_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "/");
}

void view::on_pushButton_parend_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + ")");
}

void view::on_pushButton_pow_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "^");
}

void view::on_pushButton_parstart_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "(");
}

void view::on_pushButton_mod_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "mod");
}

void view::on_pushButton_x_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "x");
}

void view::on_pushButton_acos_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "acos");
}

void view::on_pushButton_cos_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "cos");
}

void view::on_pushButton_asin_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "asin");
}

void view::on_pushButton_sin_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "sin");
}

void view::on_pushButton_atan_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "atan");
}

void view::on_pushButton_tan_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "tan");
}

void view::on_pushButton_sqrt_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "sqrt");
}

void view::on_pushButton_ln_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "ln");
}

void view::on_pushButton_log_clicked() {
  ui->lineEdit->setText(ui->lineEdit->text() + "log");
}

void view::on_pushButton_eq_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);
  QString lineedit_text = ui->lineEdit->text();
  std::string expression = lineedit_text.toStdString();
  double x = 0.0;
  if ((ui->lineEdit_3->text().isEmpty()) &&
      (expression.find("x") != std::string::npos)) {
    ui->lineEdit->setText("bad expression");
  } else {
    QString lineedit3_text = ui->lineEdit_3->text();
    x = lineedit3_text.toDouble();
    try {
      s21::controller controller;
      double result = controller.getResult(expression, x);
      ui->lineEdit->setText(QString::number(result));
    } catch (const std::invalid_argument& e) {
      ui->lineEdit->setText("bad expression");
    }
  }
}

void view::on_pushButton_graph_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  QString xminText = ui->lineEdit_xmin->text();
  bool convXmin;
  double xmin = xminText.toDouble(&convXmin);

  QString xmaxText = ui->lineEdit_xmax->text();
  bool convXmax;
  double xmax = xmaxText.toDouble(&convXmax);

  QString yminText = ui->lineEdit_ymin->text();
  bool convYmin;
  double ymin = yminText.toDouble(&convYmin);

  QString ymaxText = ui->lineEdit_ymax->text();
  bool convYmax;
  double ymax = ymaxText.toDouble(&convYmax);

  if (convXmin && convXmax && convYmin && convYmax) {
    ui->widget->xAxis->setRange(xmin, xmax);
    ui->widget->yAxis->setRange(ymin, ymax);
    QVector<double> xData, yData;
    double shag = (xmax - xmin) / 100;
    QString lineedit_text = ui->lineEdit->text();
    std::string expression = lineedit_text.toStdString();
    try {
      for (double x = xmin; x < xmax; x += shag) {
        s21::controller controller;
        double y = controller.getResult(expression, x);
        if (std::isfinite(y) && !std::isnan(y)) {
          xData.push_back(x);
          yData.push_back(y);
        }
      }
      ui->widget->addGraph();
      ui->widget->graph(0)->setPen(QPen(Qt::blue));
      ui->widget->graph(0)->setData(xData, yData);
      ui->widget->replot();
    } catch (const std::invalid_argument& e) {
      ui->lineEdit->setText("");
      ui->widget->removeGraph(0);
      ui->widget->replot();
    }
  } else {
    ui->widget->removeGraph(0);
    ui->widget->replot();
  }
}

void view::on_pushButton_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  ui->tableWidget->clear();
  ui->tableWidget->setRowCount(0);
  ui->tableWidget->setColumnCount(5);

  QString loanText = ui->lineEdit_loan->text();
  bool convLoan;
  double loan = loanText.toDouble(&convLoan);

  QString monthsText = ui->lineEdit_months->text();
  bool convMonths;
  int months = monthsText.toInt(&convMonths);
  int comboBox_2 = ui->comboBox_2->currentIndex();
  if (comboBox_2 == 1) months *= 12;

  QString interestText = ui->lineEdit_interest->text();
  bool convInterest;
  double interest = interestText.toDouble(&convInterest);
  if (convLoan && convMonths && convInterest && loan > 0 && months > 0 &&
      interest > 0) {
    ui->tableWidget->setRowCount(months + 2);
    QTableWidgetItem* item = new QTableWidgetItem("№");
    ui->tableWidget->setItem(0, 0, item);
    item = new QTableWidgetItem("Сумма");
    ui->tableWidget->setItem(0, 1, item);
    item = new QTableWidgetItem("Основной");
    ui->tableWidget->setItem(0, 2, item);
    item = new QTableWidgetItem("Проценты");
    ui->tableWidget->setItem(0, 3, item);
    item = new QTableWidgetItem("Остаток");
    ui->tableWidget->setItem(0, 4, item);

    s21::model_credit credit;
    int comboBox_1 = ui->comboBox->currentIndex();
    if (comboBox_1 == 0) {
      credit.credit_annuity(months, loan, interest);
      fillTable(credit, months);
    } else {
      credit.credit_diff(months, loan, interest);
      fillTable(credit, months);
    }
  }
}

void view::fillTable(const s21::model_credit& credit, int months) {
  for (int i = 0; i < months; ++i) {
    QString iTemp = QString::number(i + 1);
    QString monthlyTemp = QString::number(credit.getMonthly(i));
    QString interest_paidTemp = QString::number(credit.getInterestPaid(i));
    QString principalTemp = QString::number(credit.getPrincipal(i));
    QString remaining_loanTemp =
        QString::number(credit.getRemainingLoan(i + 1));
    QTableWidgetItem* item = new QTableWidgetItem(iTemp);
    ui->tableWidget->setItem(i + 1, 0, item);
    item = new QTableWidgetItem(monthlyTemp);
    ui->tableWidget->setItem(i + 1, 1, item);
    item = new QTableWidgetItem(principalTemp);
    ui->tableWidget->setItem(i + 1, 2, item);
    item = new QTableWidgetItem(interest_paidTemp);
    ui->tableWidget->setItem(i + 1, 3, item);
    item = new QTableWidgetItem(remaining_loanTemp);
    ui->tableWidget->setItem(i + 1, 4, item);
  }
  QString totalpaidTemp = QString::number(credit.getTotalPaid());
  QString totalinterestTemp = QString::number(credit.getTotalInterest());
  QString totalprincipalTemp = QString::number(credit.getTotalPrincipal());
  QTableWidgetItem* item = new QTableWidgetItem(totalpaidTemp);
  ui->tableWidget->setItem(months + 1, 1, item);
  item = new QTableWidgetItem(totalprincipalTemp);
  ui->tableWidget->setItem(months + 1, 2, item);
  item = new QTableWidgetItem(totalinterestTemp);
  ui->tableWidget->setItem(months + 1, 3, item);
  item = new QTableWidgetItem("Total:");
  ui->tableWidget->setItem(months + 1, 0, item);
}

void view::on_checkBox_toggled(bool checked) {
  if (checked) {
    ui->label_17->setEnabled(false);
    ui->label_17->setVisible(false);
    ui->label_18->setEnabled(true);
    ui->label_18->setVisible(true);
  } else {
    ui->label_17->setEnabled(true);
    ui->label_17->setVisible(true);
    ui->label_18->setEnabled(false);
    ui->label_18->setVisible(false);
  }
}

void view::on_checkBox_2_toggled(bool checked) {
  if (checked) {
    ui->comboBox_5->setEnabled(true);
    ui->comboBox_6->setEnabled(true);
    ui->lineEdit_2->setEnabled(true);
    ui->label_21->setEnabled(true);
  } else {
    ui->comboBox_5->setEnabled(false);
    ui->comboBox_6->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
    ui->label_21->setEnabled(false);
  }
}

void view::on_pushButton_10_clicked() {
  setlocale(LC_NUMERIC, "C");
  QLocale locale(QLocale::English);

  ui->tableWidget_2->clear();
  ui->tableWidget_2->setRowCount(0);
  ui->tableWidget_2->setColumnCount(4);

  QString depositText = ui->lineEdit_deposit->text();
  bool convDeposit;
  double deposit = depositText.toDouble(&convDeposit);

  QString monthsText = ui->lineEdit_months_2->text();
  bool convMonths;
  double months = monthsText.toDouble(&convMonths);

  QString interestText = ui->lineEdit_interest_2->text();
  bool convInterest;
  double interest = interestText.toDouble(&convInterest);

  QString taxText = ui->lineEdit_tax->text();
  bool convTax;
  double tax = taxText.toDouble(&convTax);

  int comboBox_4 = ui->comboBox_4->currentIndex();
  int frequency = 12;
  if (comboBox_4 == 0) {
    frequency = 1;
  }

  int capitalization = 0;
  if (ui->checkBox->isChecked()) {
    capitalization = frequency;
  }

  int temp = 0;
  double transfer_amount = 0.0;

  if (convDeposit && convMonths && convInterest && convTax && deposit > 0 &&
      months > 0 && interest > 0 && tax > 0) {
    int comboBox_3 = ui->comboBox_3->currentIndex();
    if (comboBox_3 == 1) months *= 12;

    int transfer_frequency = 1;
    if (ui->checkBox_2->isChecked()) {
      int comboBox_6 = ui->comboBox_6->currentIndex();
      switch (comboBox_6) {
        case 0:
          transfer_frequency = 1;
          break;
        case 1:
          transfer_frequency = 2;
          break;
        case 2:
          transfer_frequency = 6;
          break;
        case 3:
          transfer_frequency = 12;
          break;
      }
      QString transferText = ui->lineEdit_2->text();
      bool convTransfer;
      transfer_amount = transferText.toDouble(&convTransfer);
      if (!(convTransfer && transfer_amount > 0)) {
        temp = 1;
      }
    }
    if (temp == 0) {
      s21::model_deposit deposit_temp;
      std::vector<double> transfers_vector;
      for (int i = 0; i < months; ++i) {
        if ((i + 1) % transfer_frequency == 0) {
          if (ui->comboBox_5->currentIndex() == 0) {
            transfers_vector.push_back(transfer_amount);
          } else if (ui->comboBox_5->currentIndex() == 1) {
            transfers_vector.push_back(-transfer_amount);
          }
        } else {
          transfers_vector.push_back(0.0);
        }
      }
      deposit_temp.set_transfers(transfers_vector);
      try {
        deposit_temp.calc_deposit(months, deposit, interest, tax,
                                  capitalization, frequency);
        fillTable_deposit(deposit_temp, months, frequency);
      } catch (const std::invalid_argument& e) {
        ui->tableWidget_2->clear();
        ui->tableWidget_2->setRowCount(0);
        ui->tableWidget_2->setColumnCount(4);
      }
    }
  }
}

void view::fillTable_deposit(s21::model_deposit& deposit, int months,
                             int frequency) {
  std::vector<double> interest = deposit.getInterestPaid();
  std::vector<double> current = deposit.getCurrentDeposit();
  std::vector<double> transfers = deposit.getTransfers();

  ui->tableWidget_2->setRowCount(transfers.size() + 3);

  for (size_t i = 0, j = 0; i < transfers.size(); ++i) {
    QString i_temp = QString::number(i + 1);
    QString transfers_temp = QString::number(transfers[i]);
    QTableWidgetItem* item = new QTableWidgetItem(transfers_temp);
    ui->tableWidget_2->setItem(i + 1, 2, item);
    item = new QTableWidgetItem(i_temp);
    ui->tableWidget_2->setItem(i + 1, 0, item);

    if (((i + 1) % frequency == 0) || (static_cast<int>(i + 1) == months)) {
      QString interest_temp = QString::number(interest[j]);
      QString current_temp = QString::number(current[j]);
      item = new QTableWidgetItem(current_temp);
      ui->tableWidget_2->setItem(i + 1, 3, item);
      item = new QTableWidgetItem(interest_temp);
      ui->tableWidget_2->setItem(i + 1, 1, item);
      ++j;
    }
  }
  QString total_interest_temp = QString::number(deposit.getTotalInterest());
  QString total_deposit_temp = QString::number(deposit.getTotalDeposit());
  QString total_tax_temp = QString::number(deposit.getTotalTax());
  QTableWidgetItem* total = new QTableWidgetItem(total_interest_temp);
  ui->tableWidget_2->setItem(transfers.size() + 1, 1, total);
  total = new QTableWidgetItem(total_deposit_temp);
  ui->tableWidget_2->setItem(transfers.size() + 1, 3, total);
  total = new QTableWidgetItem(total_tax_temp);
  ui->tableWidget_2->setItem(transfers.size() + 2, 1, total);
  total = new QTableWidgetItem("Total:");
  ui->tableWidget_2->setItem(transfers.size() + 1, 0, total);
  total = new QTableWidgetItem("Tax:");
  ui->tableWidget_2->setItem(transfers.size() + 2, 0, total);
  total = new QTableWidgetItem("Month");
  ui->tableWidget_2->setItem(0, 0, total);
  total = new QTableWidgetItem("Interest");
  ui->tableWidget_2->setItem(0, 1, total);
  total = new QTableWidgetItem("Transfer");
  ui->tableWidget_2->setItem(0, 2, total);
  total = new QTableWidgetItem("Deposit");
  ui->tableWidget_2->setItem(0, 3, total);
}

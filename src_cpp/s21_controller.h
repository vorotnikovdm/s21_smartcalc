#ifndef S21_CONTROLLER_H
#define S21_CONTORLLER_H

#include "s21_model.h"

namespace s21 {
class controller {
 public:
  double getResult(const std::string& expression, double x);
};
};  // namespace s21

#endif

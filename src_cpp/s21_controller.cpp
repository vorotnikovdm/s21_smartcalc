#ifndef S21_CONTROLLER_CPP
#define S21_CONTROLLER_CPP

#include "s21_controller.h"

namespace s21 {

double controller::getResult(const std::string& expression, double x) {
  s21::model model_temp;
  model_temp.setX(x);
  model_temp.setExpression(expression);
  model_temp.validator();
  model_temp.changeX();
  model_temp.parser();
  model_temp.shuntingYard();
  model_temp.calcPostfix();
  return model_temp.result();
}

};  // namespace s21

#endif

#ifndef S21_MODEL_H
#define S21_MODEL_H

#include <cctype>
#include <cmath>
#include <cstring>
#include <iostream>
#include <stack>
#include <string>

namespace s21 {
class model {
 public:
  // functions
  model() = default;
  ~model() = default;

  // other
  void setX(double x);
  void setExpression(std::string infix);
  void changeX();
  void validator();
  void parser();
  int isUnary(char ch);
  void shuntingYard();
  int getPrecedence(char oper);
  int postfixCheck(std::string postfix);
  void calcPostfix();
  double result();

  double getX();
  std::string getExpression();

 private:
  std::stack<char> operators_;
  std::stack<double> operands_;
  double x_;
  double result_;
  std::string expression_;
};
};  // namespace s21

#endif

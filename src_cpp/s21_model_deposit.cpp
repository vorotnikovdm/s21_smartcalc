#ifndef S21_MODEL_DEPOSIT_CPP
#define S21_MODEL_DEPOSIT_CPP

#include "s21_model_deposit.h"

namespace s21 {

void model_deposit::calc_deposit(int months, double deposit, double interest,
                                 double tax, int capitalization,
                                 int frequency) {
  total_deposit = deposit;
  current_deposit.push_back(deposit);
  double temp = 0, tax_check = 0;
  double tax_min = 1000000 * 16 / 100;
  for (int i = 0, j = 0; i < months; ++i) {
    temp += (current_deposit.at(j)) * (interest / 100 / 12);
    current_deposit.at(j) += transfers.at(i);
    if (current_deposit.at(j) < 0) {
      throw std::invalid_argument("bad expression");
    }
    if ((capitalization > 0) &&
        (((i + 1) % capitalization == 0) || (i + 1 == months))) {
      current_deposit.at(j) += temp;
      current_deposit.push_back(current_deposit.at(j));
      interest_paid.push_back(temp);
      total_deposit += temp;
      total_interest += temp;
      tax_check += temp;
      temp = 0;
      ++j;
    } else if (capitalization == 0) {
      if (((i + 1) % frequency == 0) || (i + 1 == months)) {
        current_deposit.push_back(current_deposit.at(j));
        interest_paid.push_back(temp);
        total_interest += temp;
        tax_check += temp;
        temp = 0;
        ++j;
      }
    }
    if (((i + 1) % 12 == 0) || (i + 1 == months)) {
      if (tax_check > tax_min) {
        total_tax += (tax_check - tax_min) * tax / 100;
      }
      tax_check = 0.0;
    }
  }
  total_deposit = current_deposit.back();
}

void model_deposit::set_transfers(const std::vector<double>& temp) {
  transfers = temp;
}

double model_deposit::getTotalInterest() { return total_interest; }

double model_deposit::getTotalDeposit() { return total_deposit; }

double model_deposit::getTotalTax() { return total_tax; }

std::vector<double> model_deposit::getInterestPaid() { return interest_paid; }

std::vector<double> model_deposit::getCurrentDeposit() {
  return current_deposit;
}

std::vector<double> model_deposit::getTransfers() { return transfers; }

};  // namespace s21

#endif

#ifndef S21_MODEL_CREDIT_CPP
#define S21_MODEL_CREDIT_CPP

#include "s21_model_credit.h"

namespace s21 {

void model_credit::credit_annuity(int months, double loan, double interest) {
  double monthInterest = interest / 100 / 12;
  double monthly_temp =
      (monthInterest * loan) / (1 - pow(1 + monthInterest, -months));
  remaining_loan.push_back(loan);
  for (int i = 0; i < months; ++i) {
    double interest_payment =
        round(remaining_loan[i] * monthInterest * 100) / 100;
    double principal_payment =
        round((monthly_temp - interest_payment) * 100) / 100;
    interest_paid.push_back(interest_payment);
    principal.push_back(principal_payment);
    remaining_loan.push_back(
        round((remaining_loan[i] - principal_payment) * 100) / 100);
    monthly.push_back(monthly_temp);
    total_paid += monthly_temp;
    total_interest += interest_payment;
    total_principal += principal_payment;
  }
}

void model_credit::credit_diff(int months, double loan, double interest) {
  double monthly_temp = loan / months;
  remaining_loan.push_back(loan);
  for (int i = 0; i < months; ++i) {
    double monthInterest = remaining_loan[i] * (interest / 100 / 12);
    remaining_loan.push_back(remaining_loan[i] - monthly_temp);
    monthly.push_back(monthly_temp + monthInterest);
    principal.push_back(monthly_temp);
    interest_paid.push_back(monthInterest);
    total_principal += monthly_temp;
    total_interest += monthInterest;
  }
  total_paid = total_principal + total_interest;
}

double model_credit::getMonthly(int index) const { return monthly.at(index); }
double model_credit::getInterestPaid(int index) const {
  return interest_paid.at(index);
}
double model_credit::getPrincipal(int index) const {
  return principal.at(index);
}
double model_credit::getRemainingLoan(int index) const {
  return remaining_loan.at(index);
}
double model_credit::getTotalPaid() const { return total_paid; }
double model_credit::getTotalInterest() const { return total_interest; }
double model_credit::getTotalPrincipal() const { return total_principal; }

};  // namespace s21

#endif

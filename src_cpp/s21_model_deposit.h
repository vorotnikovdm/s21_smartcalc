#ifndef S21_MODEL_DEPOSIT_H
#define S21_MODEL_DEPOSIT_H

#include <iostream>
#include <vector>

namespace s21 {
class model_deposit {
 public:
  model_deposit() : total_interest(0), total_deposit(0), total_tax(0) {}
  ~model_deposit() = default;

  void calc_deposit(int months, double deposit, double interest, double tax,
                    int capitalization, int frequency);
  void set_transfers(const std::vector<double>& temp);

  double getTotalInterest();
  double getTotalDeposit();
  double getTotalTax();
  std::vector<double> getInterestPaid();
  std::vector<double> getCurrentDeposit();
  std::vector<double> getTransfers();

 private:
  std::vector<double> interest_paid;
  std::vector<double> current_deposit;
  std::vector<double> transfers;
  double total_interest;
  double total_deposit;
  double total_tax;
};
}  // namespace s21

#endif

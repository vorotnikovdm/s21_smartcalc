#ifndef S21_MODEL_CPP
#define S21_MODEL_CPP

#include "s21_model.h"

namespace s21 {

void model::setX(double x) { x_ = x; }

void model::setExpression(std::string infix) { expression_ = infix; }

void model::changeX() {
  size_t pos = expression_.find("x");
  while (pos != std::string::npos) {
    expression_.replace(pos, 1, "(" + std::to_string(x_) + ")");
    pos = expression_.find("x");
  }
}

void model::validator() {
  int count = 0, error = 0;
  for (size_t i = 0; i < expression_.size(); ++i) {
    char token = expression_[i];
    if (token == '(') {
      ++count;
    } else if (token == ')') {
      --count;
    }
    if (std::isdigit(token)) {
      if (expression_[i + 1] == ' ') {
        error = 0;
      } else if (expression_[i + 1] == '.') {
        error = 0;
      } else if (std::isdigit(expression_[i + 1])) {
        error = 0;
      } else if (std::strchr("+-*/^m)", expression_[i + 1]) != NULL) {
        error = 0;
      } else {
        error = 1;
      }
    }
    if (error == 1) {
      break;
    }
  }
  if (count != 0) {
    error = 1;
  }
  if (error == 1) {
    throw std::invalid_argument("bad expression");
  }
}

void model::parser() {
  int error = 0;
  std::string temp;
  for (size_t i = 0; i < expression_.size(); ++i) {
    char token = expression_[i];
    if (std::isdigit(token) || (token == '.') || (token == ' ') ||
        (token == '(') || (token == ')') ||
        (std::strchr("*/^", token) != NULL)) {
      temp.push_back(token);
    } else if (token == '+' || token == '-') {
      if (i == 0 || (i > 0 && (isUnary(expression_[i - 1]) ||
                               expression_[i - 1] == '('))) {
        if (token == '+')
          temp.push_back('k');
        else
          temp.push_back('l');
      } else {
        temp.push_back(token);
      }
    } else if (std::isalpha(token)) {
      if ((token == 'm') && (expression_[i + 1] == 'o') &&
          (expression_[i + 2] == 'd')) {
        temp.push_back(token);
        i += 2;
      } else if ((token == 'c') && (expression_[i + 1] == 'o') &&
                 (expression_[i + 2] == 's')) {
        temp.push_back(token);
        i += 2;
      } else if ((token == 's') && (expression_[i + 1] == 'i') &&
                 (expression_[i + 2] == 'n')) {
        temp.push_back(token);
        i += 2;
      } else if ((token == 't') && (expression_[i + 1] == 'a') &&
                 (expression_[i + 2] == 'n')) {
        temp.push_back(token);
        i += 2;
      } else if ((token == 'a') && (expression_[i + 1] == 'c') &&
                 (expression_[i + 2] == 'o') && (expression_[i + 3] == 's')) {
        temp.push_back('x');
        i += 3;
      } else if ((token == 'a') && (expression_[i + 1] == 's') &&
                 (expression_[i + 2] == 'i') && (expression_[i + 3] == 'n')) {
        temp.push_back(token);
        i += 3;
      } else if ((token == 'a') && (expression_[i + 1] == 't') &&
                 (expression_[i + 2] == 'a') && (expression_[i + 3] == 'n')) {
        temp.push_back('r');
        i += 3;
      } else if ((token == 's') && (expression_[i + 1] == 'q') &&
                 (expression_[i + 2] == 'r') && (expression_[i + 3] == 't')) {
        temp.push_back('q');
        i += 3;
      } else if ((token == 'l') && (expression_[i + 1] == 'n')) {
        temp.push_back('n');
        i += 1;
      } else if ((token == 'l') && (expression_[i + 1] == 'o') &&
                 (expression_[i + 2] == 'g')) {
        temp.push_back('o');
        i += 2;
      } else
        error = 1;
    } else
      error = 1;
  }
  setExpression(temp);
  if (error == 1) {
    throw std::invalid_argument("bad expression");
  }
}

int model::isUnary(char ch) { return ch == '+' || ch == '-'; }

void model::shuntingYard() {
  std::string temp;
  for (size_t i = 0; i < expression_.size(); ++i) {
    char token = expression_[i];
    if (std::isdigit(token) || (token == '.')) {
      while (std::isdigit(token) || token == '.') {
        temp.push_back(token);
        ++i;
        token = expression_[i];
      }
      temp.push_back(' ');
      i--;
    } else if (token == '(') {
      operators_.push(token);
    } else if (token == ')') {
      while (!(operators_.empty()) && (operators_.top() != '(')) {
        temp.push_back(operators_.top());
        operators_.pop();
        temp.push_back(' ');
      }
      operators_.pop();
    } else if (std::strchr("+-*/^msctaxrqnokl", token) != NULL) {
      while (!(operators_.empty()) &&
             getPrecedence(token) <= getPrecedence(operators_.top())) {
        temp.push_back(operators_.top());
        temp.push_back(' ');
        operators_.pop();
      }
      operators_.push(token);
    }
  }
  while (!(operators_.empty())) {
    temp.push_back(operators_.top());
    temp.push_back(' ');
    operators_.pop();
  }
  if (postfixCheck(temp) == 1) {
    throw std::invalid_argument("bad expression");
  } else {
    setExpression(temp);
  }
}

int model::getPrecedence(char oper) {
  switch (oper) {
    case '+':
    case '-':
    case 'k':
    case 'l':
      return 1;
    case '*':
    case '/':
    case 'm':
      return 2;
    case '^':
      return 3;
    case 's':
    case 'c':
    case 't':
    case 'a':
    case 'x':
    case 'r':
      return 4;
    case 'n':
    case 'o':
      return 5;
    case 'q':
      return 6;
    default:
      return 0;
  }
}

int model::postfixCheck(std::string postfix) {
  int error = 0;
  for (size_t i = 0; i < postfix.size(); ++i) {
    char token = postfix[i];
    if (token == '.') {
      if (std::strchr("1234567890", postfix[i - 1]) == NULL) {
        error = 1;
        break;
      } else {
        while (1) {
          i++;
          if (std::isdigit(postfix[i]))
            error = 0;
          else if (postfix[i] == ' ') {
            error = 0;
            break;
          } else if (postfix[i] == '.') {
            error = 1;
            break;
          }
        }
      }
      if (error == 1) break;
    }
  }
  return error;
}

void model::calcPostfix() {
  int error = 0;
  for (size_t i = 0; i < expression_.size(); ++i) {
    char token = expression_[i];
    if (std::isdigit(token))
    //|| (token == '.' && std::isdigit(expression_[i + 1])))
    {
      std::string temp;
      while (std::isdigit(expression_[i]) || (expression_[i] == '.')) {
        temp.push_back(token);
        ++i;
        token = expression_[i];
      }
      double operand = std::stod(temp);
      operands_.push(operand);
      --i;
    } else if (token == ' ') {
      continue;
    } else {
      double operand2 = 0.0, operand1 = 0.0;
      switch (token) {
        case '+':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(operand1 + operand2);
          break;
        case '-':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(operand1 - operand2);
          break;
        case '*':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(operand1 * operand2);
          break;
        case '/':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          if (operand2 == 0.0) {
            error = 1;
            break;
          }
          operands_.push(operand1 / operand2);
          break;
        case '^':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::pow(operand1, operand2));
          break;
        case 'm':
          operand2 = operands_.top();
          operands_.pop();
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::fmod(operand1, operand2));
          break;
        case 's':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::sin(operand1));
          break;
        case 'c':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::cos(operand1));
          break;
        case 't':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::tan(operand1));
          break;
        case 'a':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::asin(operand1));
          break;
        case 'x':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::acos(operand1));
          break;
        case 'r':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::atan(operand1));
          break;
        case 'q':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::sqrt(operand1));
          break;
        case 'n':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::log(operand1));
          break;
        case 'o':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(std::log10(operand1));
          break;
        case 'k':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(operand1 * 1);
          break;
        case 'l':
          operand1 = operands_.top();
          operands_.pop();
          operands_.push(-1 * operand1);
          break;
        default:
          error = 1;
          break;
      }
    }
  }
  if (operands_.size() != 1) {
    error = 1;
  }
  if (error == 1) {
    throw std::invalid_argument("bad expression");
  } else {
    result_ = operands_.top();
  }
}

double model::result() { return result_; }

double model::getX() { return x_; }

std::string model::getExpression() { return expression_; }

};  // namespace s21

#endif
